
Description:
------------
This module adds functionality to the Webform module to submit Leads into 
Capsule CRM.  

http://capsulecrm.com

Capsule offers a free edition of their CRM platform for up to 2 users.  

You can find your Webform API key by logging into your Capsule Account, 
going to Settings, Integrations, Website Contact Form.  Your Web to 
Lead form API key will be available on that page.

If you are having issues with blank screens or errors on form submissions,
as a first troubleshooting step, enable the debugging flag in the module 
settings screen.

This module was sponsored by appendTo, LLC (http://appendto.com) and built
by Mike Hostetler (http://mike-hostetler.com).

This module is heavily based on the SalesforceWebform module, which in 
turn was based on the sugarwebform module.
